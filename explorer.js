#!/usr/bin/env node
var cli= require('./lib/cli.js');
var logger = require('./logger');
const commandLineArgs = require('command-line-args')

console.log('welcome to the blockchain explorer');

const optionDefinitions = [
  { name: 'start-block', alias: 's', type: Number},
  { name: 'end-block', type: Number},
  { name: 'blocks-back',  type: Number },
  { name: 'ganache', type:Boolean }
]
const options = commandLineArgs(optionDefinitions)

if(options['ganache']){
    process.env.GANACHE = true;
}
if(options['start-block']!==undefined && options['end-block']!==undefined){
    cli.getBlocksBetweenNumbers(options['start-block'],options['end-block']);
    return;
}
else if(options['blocks-back']){
    cli.getBlocksFromNumber(options['blocks-back']);
    return;
}
else{
    console.log('you must specify either --start-block and --end-block or --blocks-back');
}
