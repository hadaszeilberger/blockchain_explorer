Introduction
-----------
The purpose of this tool is to explore transactions on the block chain. The user specified a block range either by giving a start point and an endpoint or by
giving how many blocks back from the current position and the cli will print the following:
        - which addresses received money and how much they received,  
        - which addresses sent money and how much they sent,    
        - the number of contract addresses   
        - the percentage of addresses that were contracts,  
        - the number of unique addresses.   

Install
------
`git clone https://hadaszeilberger@bitbucket.org/hadaszeilberger/blockchain_explorer.git`  
`cd blockchain_explorer`  
`npm install`  
`npm install -g `(need to do npm install first)  

Run CLI
------
`explore --start-block {block number to begin with} --end-block {block number to end with}`  
`explore --blocks-back {number of blocks back}`  

Example: `explore --start-block 500 --end-block 600`  
Example: `explore --blocks-back 60`  

Test
----
`npm test`  
