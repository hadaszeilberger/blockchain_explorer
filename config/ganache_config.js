var path = require('path');
var fs = require('fs');
var solc = require('solc');
var abiDecoder = require('abi-decoder');
var web3 = require('./web3_config.js');
var logger = require('../logger');
var wei_converter = require('../api/wei_converter.js');
//send transactions from existing accounts
//If there are no existing accounts, then create accounts
//Make a test suite for this as well.
//
function saveDataToFile(relativePath,data){
    var file = path.normalize(__dirname + relativePath);
    fs.writeFile(
        file,'module.exports=' + data,
        (err=>{
            if(err){
                throw err;
            }
        })
    );
}
function getSendTransactionPromises(accounts){
    var promises = new Array();
    for(var i=0;i<accounts.length-1;i++){
        promises.push(web3.eth.sendTransaction({from:accounts[i],to:accounts[i+1],value:wei_converter.toWei(i)}))
    }
    return promises;
}
function sendTransactions(){
    var promises = new Array();
    return web3.eth.getAccounts().
        then((accounts)=>{
            logger('accounts',accounts);
            return Promise.all(getSendTransactionPromises(accounts));
        }).then((transactionObjects)=>{
            return transactionObjects;
        }).catch((err)=>{
            logger('error sending transactions','config/ganache_config');
            return Promise.reject(err);
        }).catch((err)=>{
            logger('error getting accounts','config/ganache_config');
            return Promise.reject(err);
        })
}
function smartContract(){
    var input = fs.readFileSync(__dirname + '/contracts/token.sol');
    var output = solc.compile(input.toString(),1);
    var bytecode = output.contracts[':Token'].bytecode;
    var abi = JSON.parse(output.contracts[':Token'].interface);
    return {
        bytecode:bytecode,
        abi:abi
    }
}
function createContract(){
   return web3.eth.getAccounts().
        then((accounts)=>{
            var contractAddress = accounts[0];
            var smart_contract = smartContract();
            var contract = new web3.eth.Contract(smart_contract.abi);
            return contract.deploy({data:smart_contract.bytecode})
                .send({
                    from:contractAddress,
                    gas:1500000,
                    gasPrice:'30000000000'
                })
                .on('transactionHash',(transactionHash)=>{
                    web3.eth.getTransaction(transactionHash).then(transObj=>{
                        var file = '/../lib/tests/data/contract_creation.js';
                        saveDataToFile(file,JSON.stringify([transObj]));
                    })
                }).then(function(newContractInstance){
                        var file = '/../api/tests/data/contractAddress.js';
                        saveDataToFile(file,JSON.stringify([newContractInstance.options.address]));
                        return newContractInstance.options.address;
                }).catch(err=>{
                    console.log(err);
                    return err;
                })
        }).catch(err=>{
            console.log(err);
            return err;
        })
}
module.exports={
    createContract:createContract,
    sendTransactions:sendTransactions
}

