var logger=require('../logger.js');
var infura_config = require('./infura_config.js');
var Web3 = require('web3');
var ganache = require('ganache-cli');
connection_string = 'https://mainnet.infura.io/v3/' + infura_config.api_key;
var web3;
if(process.env.GANACHE){
    web3 = new Web3(ganache.provider({
        total_accounts:100,
        db_path:'./blockchain',
    }))
}
else{
   web3 = new Web3(new Web3.providers.HttpProvider(connection_string));
}
module.exports = web3;
