var mocha = require('mocha');
var assert = require('assert');
var web3 = require('../web3_config.js');

describe('connection to ethereum node via infura using web3 for RPC requests',function(){
    it('should output the correct provider',function(){
        if(!process.env.GANACHE){
            assert.equal(web3.eth.currentProvider.host.split('.')[1],'infura');
        }
    });
})
