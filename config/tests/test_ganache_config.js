var mocha = require('mocha');
var ganache_config = require('../ganache_config.js');
var assert = require('assert');
describe('sets up blockchain by sending transactions between every two consecutive accounts',()=>{
    it('should return an array of 99 transaction hashes',function(done){
        this.timeout(15000);
        ganache_config.sendTransactions().then((hashes)=>{
            assert.equal(hashes.length,99);
            done();
        }).catch((err)=>{
            console.log(err);
            done(new Error());
        })
    })
})
describe('contract creation transaction',()=>{
    it('should create a contract',function(done){
        this.timeout(15000);
        ganache_config.createContract().then(response=>{
            assert.equal(response.slice(0,2),'0x');
            done();
        }).catch(err=>{
            console.log('err');
            console.log(err);
            done(new Error());
        })
    })

})
