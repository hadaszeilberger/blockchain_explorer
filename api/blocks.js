var web3 = require('../config/web3_config');
var logger = require('../logger.js');
function getBlock(blockNumber){
    return web3.eth.getBlock(blockNumber,true).
        then(block=>{
            if(!block){
                return Promise.reject(new Error('block doesnt exist'));
            }
            return {
                number:block.number,
                transactions:block.transactions,
                uncles:block.uncles
            };
        })
        .catch(err=>{
            logger('error getting block with number ' + blockNumber,err);
            return Promise.reject({
                message:'error getting block with number' + blockNumber,
                error:err
            });
        });
}

function getCurrentBlockNumber(){
    return web3.eth.getBlockNumber()
        .then((block)=>{
            return block;
        })
        .catch((err)=>{
            logger('error getting current block number',err);
            return err;
        })
}

function getBlocksBetweenNumbers(startNumber,endNumber){
    var blockPromises = new Array();
    for(var i=startNumber;i<endNumber;i++){
        blockPromises.push(getBlock(i));
    }
    return Promise.all(blockPromises);
}

module.exports={
    getBlocksBetweenNumbers:getBlocksBetweenNumbers,
    getCurrentBlockNumber : getCurrentBlockNumber
}
if(process.env.TEST){
    module.exports.getBlock = getBlock;

}
