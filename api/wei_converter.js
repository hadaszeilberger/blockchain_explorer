var web3 = require('../config/web3_config.js');
var logger = require('../logger.js');

function fromWei(weiVal){
    return parseInt(web3.utils.fromWei(weiVal));
}
function toWei(etherVal){
    return web3.utils.toWei(JSON.stringify(etherVal),'ether')
}
module.exports={
    fromWei:fromWei,
    toWei:toWei
}
