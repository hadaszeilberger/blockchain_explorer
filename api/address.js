var web3 = require('../config/web3_config.js');
var logger = require('../logger.js');

function isContract(address){
    return web3.eth.getCode(address).
        then((code)=>{
            isContract = code!=='0x' ? true : false;
            return {
                address:address,
                isContract:isContract
            };
        }).
        catch(err=>{
            logger('error getting code for address ' + address,err);
            return err;
        })
}
function identifyContracts(addresses){
    console.log(addresses);
    var promises = new Array();
    for(var i=0;i<addresses.length;i++){
        promises.push(isContract(addresses[i]));
    }
    return Promise.all(promises).then((results)=>{
        return results;
    }).catch((err)=>{
        logger('error in identify contracts','identifyContracts');
        return err;
    })
}

function getContractAddress(transactionHash){
        return web3.eth.getTransactionReceipt(transactionHash)
        .then(transactionObj=>{
            if(!transactionObj){
                logger('error getting transaction object','getContractAddress');
                return Promise.reject(new Error('transaction object doesnt exist for this hash'));
            }
            return {
                transactionHash:transactionHash,
                contractAddress:transactionObj.contractAddress
            }
        })
        .catch(err=>{
            console.log('catch 1');
            logger('error getting transaction object','get contract address');
            return Promise.reject(err);
        })
};


function getContractAddresses(transactionHashes){
    var promises = new Array();
    for(var i=0;i<transactionHashes.length;i++){
        promises.push(getContractAddress(transactionHashes[i]));
    }
    return Promise.all(promises).
        then(addresses=>{
            return addresses;
        }).
        catch(err=>{
            console.log('catch 2');
            logger('error getting addresses for all hashes',err);
            return Promise.reject(err);
        })
}
module.exports={
    identifyContracts:identifyContracts,
    getContractAddresses:getContractAddresses
};


