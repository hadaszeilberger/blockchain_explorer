var mocha = require('mocha');
var address = require('../address.js');
var assert = require('assert');
var logger = require('../../logger.js');
var contract_address = require('./data/contractAddress.js');
var non_contract_address = ['0xe2D66253AE8569Ef1C211E2515a227FdC751BcF8'];
var contract_creation = require('../../lib/tests/data/contract_creation.js');

describe('uses getTransactionReceipt to get contract address for hash associated with transaction with null "to" field',()=>{
    it('should return an object with hash and contract address',function(done){
        var transactionHashes = [contract_creation[0].hash];
        address.getContractAddresses(transactionHashes).then((response)=>{
            assert.equal(response[0].hasOwnProperty('transactionHash'),true);
            assert.equal(response[0].hasOwnProperty('contractAddress'),true);
            done();
        }).catch((error)=>{
            console.log(error);
            done(new Error());
        })
    });
})
