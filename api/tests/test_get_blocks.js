var mocha=require('mocha');
var assert = require('assert');
var blocks = require('../blocks.js');
var logger = require('../../logger.js');
describe('get blocks info using web3.js',function(){
    it('should get the current block number',function(done){
        blocks.getCurrentBlockNumber().then((blockNumber)=>{
            assert.equal(typeof(blockNumber),'number');
            done();
        }).catch((error)=>{
            done(new Error());
        })
    })
    it('should throw error for nonexistant block',function(done){
        blocks.getBlock(-1).then((block)=>{
            done(new Error());
        }).catch((err)=>{
            done();
        })
    })

    it('should get blocks between 90 and 95',function(done){
        blocks.getBlocksBetweenNumbers(90,95).then((blocks)=>{
            assert.equal(blocks.length,5);
            done();
        }).catch((err)=>{
            console.log(err);
            done();
        })
    })
})
