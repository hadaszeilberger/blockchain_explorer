var mocha = require('mocha');
var assert = require('assert');
var tasks = require('../tasks.js');
var logger = require('../../logger.js');
var transactions = require('./data/transactions.js');
var contract_creation_transaction = require('./data/contract_creation.js');//contract creation transaction should be an actual contract creation transaction
var block= require('./data/block.js');

describe('get transactions from blocks',()=>{
    it('should get array of transactions associated with block',()=>{
        assert.equal(tasks.getTransactions(block).hasOwnProperty('length'),true);
    })
})
describe('get stats from transactions',()=>{
    it('should upsert address',function(){
        var addressObj = {"totalAmountTransferred":0};
        addressObj = tasks.upsertAddress('0x315258A29603581c1C93a94ef14640ec4ce5907c','received',5,addressObj);
        assert.equal(addressObj['0x315258A29603581c1C93a94ef14640ec4ce5907c']['received'],5);
    })
    it('should deal with a null "to" value by inserting transaction hash instead of address and adding a contract_creation property',function(){
        var addressObj = {"totalAmountTransferred":0};
        var addressObj = tasks.accumulateStats(contract_creation_transaction,addressObj);
        assert.equal(addressObj[contract_creation_transaction[0]['hash']].contract_creation,true);
    })
    it('should return an object with stats',function(){
        var addressObj = {"totalAmountTransferred":0};
        var addressObj = tasks.accumulateStats(transactions,addressObj);
        assert.equal(addressObj.totalAmountTransferred,2);
    })
})
describe('should get blocks in batches up to limit',()=>{
    it('should successfully get blocks in batches and return an object of addresses',function(done){
        var addressObj = {"totalAmountTransferred":0};
        tasks.getBlocksBetweenNumbers(10,85,addressObj).then((response)=>{
            assert.equal(Object.keys(response).length>0,true);
            done();
        }).catch((err)=>{
            console.log(err);
            done(new Error());
        })
    })
    it('should get 5 blocks back from current block',(done)=>{
        var addressObj = {"totalAmountTransferred":0};
        tasks.getBlocksFromCurrPosition(5,addressObj).then((blocks)=>{
            assert.equal(Object.keys(block).length>0,true);
            done();
        }).catch((err)=>{
            console.log(err);
            done(new Error());
        })
    })
})
