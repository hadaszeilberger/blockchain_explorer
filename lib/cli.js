var tasks = require('./tasks.js');
var logger = require('../logger');
function getStats(addressObj){
    var received = new Array();
    var sent = new Array();
    var contractCreation = new Array();
    for (var address in addressObj){
        if(addressObj[address].sent > 0){
            sent.push({address:address,sent:addressObj[address].sent});
        }
        if(addressObj[address].received>0){
            received.push({address:address,received:addressObj[address].received});
        }
        if(addressObj[address]['contract_creation']===true){
            contractCreation.push(address);
        }
    }
    return{
        sent:sent,
        received:received,
        contractCreation:contractCreation
    }
}
function summarize(addressObj){
    var stats = getStats(addressObj);
    var uniqueAddresses = Object.keys(addressObj).length;
    var percentContracts = (stats.contractCreation.length / uniqueAddresses ) * 100;
    var received = "";
    var sent = "";
    stats.received.forEach(receivedAddr=>{
        received = received.concat('\n' + JSON.stringify(receivedAddr));
    })
    stats.sent.forEach(sentAddr=>{
        sent = sent.concat('\n' + JSON.stringify(sentAddr));
    })

    console.log('Addresses that sent money ' + '\n' + sent + '\n\n' +'Addresses that received money' + '\n' + received + '\n\n' + "SUMMARY" + '\n' + 'Total Amount Transferred: ' + addressObj.totalAmountTransferred + '\n' +'Number of Unique Addresses: ' + uniqueAddresses + '\n' + 'Number of Addresses that are Contracts: ' + stats.contractCreation.length + '\nPercent of Addresses that are Contracts: ' + percentContracts + '%\n') ;

}
function getBlocksBetweenNumbers(start,end){
    var addressObj = {"totalAmountTransferred":0};
    tasks.getBlocksBetweenNumbers(start,end,addressObj)
    .then((addressObj)=>{
        return tasks.replaceTransactionHashes(addressObj)
    }).then((addressObj)=>{
        summarize(addressObj);
    }).catch((err)=>{
        logger('error getting stats','explorer.js');
        console.log(err);
    }).catch((err)=>{
        logger('error getting block','explorer.js');
        console.log(err);
    })
}
function getBlocksFromNumber(num){
    var addressObj = {"totalAmountTransferred":0};
    tasks.getBlocksFromCurrPosition(num,addressObj)
    .then((addressObj)=>{
        return tasks.replaceTransactionHashes(addressObj)
    }).then((addressObj)=>{
        summarize(addressObj);
    }).catch((err)=>{
        logger('error getting stats','explorer.js');
        console.log(err);
    }).catch((err)=>{
        logger('error getting block','explorer.js');
        console.log(err);
    })
}

module.exports={
    getBlocksBetweenNumbers:getBlocksBetweenNumbers,
    getBlocksFromNumber:getBlocksFromNumber
}
