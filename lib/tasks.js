var blocks = require('../api/blocks.js');
var wei_converter=require('../api/wei_converter.js');
var logger = require('../logger');
var address = require('../api/address.js')
function getTransactions(block){
    return block.transactions;
}
function accumulateStats(transactions,addressObj){
    for(var i=0;i<transactions.length;i++){
        var transaction = transactions[i];
        var value = wei_converter.fromWei(transactions[i].value);
        addressObj['totalAmountTransferred'] += value;
        addressObj = transaction.to ? upsertAddress(transaction.to,'received',value,addressObj) : upsertAddress(transaction.hash,'received',value,addressObj,true);
        addressObj = upsertAddress(transaction.from,'sent',value,addressObj);
    }
    return addressObj;
}
function upsertAddress(address,action,amount,addressObj,isContractCreation){
    if(!addressObj[address]){
        addressObj[address] = {"sent":0,"received":0}
    }
    addressObj[address][action]+=amount;
    if(isContractCreation){
        addressObj[address]['contract_creation'] = true;
    }
    return addressObj;
}

function processBlocks(blocks,addressObj){
    for(var i=0;i<blocks.length;i++){
        var transactions = getTransactions(blocks[i]);
        addressObj = accumulateStats(transactions,addressObj);
    }
    return addressObj;
}

function getBlocksBetweenNumbers(startBlock,endBlock,addressObj){
    var limit = 156897;
    var stopPoint= startBlock + limit > endBlock ? endBlock : startBlock + limit;
    if(startBlock === endBlock){
        return addressObj;
    }
    return blocks.getBlocksBetweenNumbers(startBlock,stopPoint)
        .then((blocks)=>{
            return getBlocksBetweenNumbers(stopPoint,endBlock,processBlocks(blocks,addressObj));
        }).then((response)=>{
            logger('finished populating address object','lib/tasks.js');
            return response;
        }).catch((err)=>{
            logger('recursive group catch - blocksBetweenNumbers',err);
            return err;
        }).catch((err)=>{
            logger('blocks.getBlocksBetweenNumbers',err);
            return err;
        })
}

function getBlocksFromCurrPosition(numBlocks,addressObj){
    return blocks.getCurrentBlockNumber()
        .then((currentBlockNumber)=>{
            return getBlocksBetweenNumbers(currentBlockNumber-numBlocks,currentBlockNumber,addressObj);
        }).then((response)=>{
            return response;
        }).catch((err)=>{
            return err;
        });
}
function getAddressKeys(addressObj){
    return Object.keys(addressObj).slice(1,Object.keys(addressObj).length);
}
function getContractCreationHashes(addressObj){
    var transactionHashes = new Array();
    for(var address in addressObj){
        if(addressObj[address]['contract_creation']===true){
            console.log(address);
            transactionHashes.push(address);
        }
    }
    return transactionHashes;
}
function replaceTransactionHashes(addressObj){
    return address.getContractAddresses(getContractCreationHashes(addressObj))
        .then((contracts)=>{
            contracts.forEach(contract=>{
                var tempObj = Object.assign({},addressObj[contract.transactionHash]);
                addressObj[contract.contractAddress] = tempObj;
                delete addressObj[contract.transactionHash];
            })
            return addressObj;
        })
        .catch(err=>{
             logger('error getting contract creation addresses','tasks.js');
             return err;
        })
}

module.exports={
    replaceTransactionHashes:replaceTransactionHashes,
    getBlocksFromCurrPosition:getBlocksFromCurrPosition,
    getBlocksBetweenNumbers:getBlocksBetweenNumbers
}
if(process.env.TEST){
    module.exports.accumulateStats = accumulateStats;
    module.exports.upsertAddress = upsertAddress;
    module.exports.getTransactions = getTransactions;
}
