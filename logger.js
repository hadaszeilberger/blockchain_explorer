var log= require('roarr').default;

const debug = log.child({
    logLevel: 10
})
module.exports=function(message,context){
    debug({
        context:context
    },message)
}
